//
//  FURBAppDelegate.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProcessingProtocol.h"
#import "FilterProcessingProtocol.h"

@interface FURBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) id<ImageProcessingProtocol>  imageProcessor;
@property (strong, nonatomic) id<FilterProcessingProtocol> filterProcessor;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (NSArray *)loadEverything;
- (NSManagedObject *)loadFromName :(NSString *)first :(NSString *)last;

@end
