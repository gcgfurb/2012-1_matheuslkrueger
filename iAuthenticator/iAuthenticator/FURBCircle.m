//
//  FURBCircle.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBCircle.h"

@implementation FURBCircle

- (id)init
{
    x = 0;
    y = 0;
    r = 0;
    
    return self;
}

- (int)x
{
    return x;
}

- (void)setX :(int)newX
{
    x = newX;
}

- (int)y
{
    return y;
}

- (void)setY :(int)newY
{
    y = newY;
}

- (int)r
{
    return r;
}

- (void)setR :(int)newR
{
    r = newR;
}

@end
