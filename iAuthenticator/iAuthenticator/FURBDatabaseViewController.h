//
//  FURBDatabaseViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "FURBNewViewController.h"

@interface FURBDatabaseViewController : UITableViewController
    <NSFetchedResultsControllerDelegate, FURBNewViewControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
