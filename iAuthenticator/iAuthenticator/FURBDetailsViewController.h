//
//  FURBDetailsViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FURBPerson.h"
#import "FURBEditViewController.h"

@interface FURBDetailsViewController : UIViewController
    <FURBEditViewControllerDelegate>

@property (strong, nonatomic) id detailItem;
@property (strong, nonatomic) FURBPerson *person;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *birthdateLabel;

@property (strong, nonatomic) IBOutlet UIImageView *irisSample;
@property (strong, nonatomic) IBOutlet UIImageView *irisGray;
@property (strong, nonatomic) IBOutlet UIImageView *irisRect;
@property (strong, nonatomic) IBOutlet UIImageView *irisCode;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmented;

- (IBAction)change:(id)sender;

@end
