//
//  FURBDetailsViewController.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBDetailsViewController.h"
#import "FURBEditViewController.h"

@interface FURBDetailsViewController ()
- (void)configureView;
@end

@implementation FURBDetailsViewController
@synthesize nameLabel       = _nameLabel;
@synthesize birthdateLabel  = _birthdateLabel;
@synthesize irisSample = _irisSample;
@synthesize irisGray = _irisGray;
@synthesize irisRect = _irisRect;
@synthesize irisCode = _irisCode;
@synthesize segmented = _segmented;

@synthesize person          = _person;
@synthesize detailItem      = _detailItem;


- (void)buttonUpdateInEditViewWasPressed :(FURBEditViewController *)controller
{
    // aqui fazer as modificacoes dos valores: nome, sexo, nascimento, etc...
    _detailItem = [controller person];
    _person     = [controller person];
    [self configureView];
    
    [controller.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        _person = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.
    
    if (self.detailItem) {
        
        NSString *sex;
        if ([self.person.sex boolValue]) {
            sex = @"Male";
        } else {
            sex = @"Female";
        }
        
        self.nameLabel.text = [[NSString alloc] initWithFormat:@"%@ %@ (%@)", 
                               self.person.first,
                               self.person.surname,
                               sex];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd'/'MM'/'yyyy"];
        self.birthdateLabel.text = [formatter stringFromDate:self.person.birthdate];
        
        [self updateView:[self.segmented selectedSegmentIndex]];
    }
}

- (void)updateView :(NSInteger)param
{
    if (param == 0) {
        // right
        
        self.irisSample.image = [[UIImage alloc] initWithData:self.person.rightIrisSample];
        self.irisGray.image   = [[UIImage alloc] initWithData:self.person.rightIrisGray];
        self.irisRect.image   = [[UIImage alloc] initWithData:self.person.rightIrisRect];
        self.irisCode.image   = [[UIImage alloc] initWithData:self.person.rightIrisCode];
    } else {
        // left
        
        self.irisSample.image = [[UIImage alloc] initWithData:self.person.leftIrisSample];
        self.irisGray.image   = [[UIImage alloc] initWithData:self.person.leftIrisGray];
        self.irisRect.image   = [[UIImage alloc] initWithData:self.person.leftIrisRect];
        self.irisCode.image   = [[UIImage alloc] initWithData:self.person.leftIrisCode];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self configureView];
}

- (void)viewDidUnload
{
    [self setNameLabel:nil];
    [self setBirthdateLabel:nil];
    
    [self setIrisSample:nil];
    [self setIrisGray:nil];
    [self setIrisRect:nil];
    [self setIrisCode:nil];
    
    [self setSegmented:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)change:(id)sender {
    
    [self updateView:[self.segmented selectedSegmentIndex]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"updatePerson"]) {
        
        FURBEditViewController *editPerson = segue.destinationViewController;
        
        editPerson.delegate = self;
        editPerson.person = self.person;
    }
}

@end

