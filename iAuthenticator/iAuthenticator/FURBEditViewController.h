//
//  FURBEditViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FURBPerson.h"

@class FURBEditViewController;
@protocol FURBEditViewControllerDelegate
- (void)buttonUpdateInEditViewWasPressed :(FURBEditViewController *)controller;
@end

@interface FURBEditViewController : UITableViewController
    <UIActionSheetDelegate>
{
    NSString *first;
    NSString *surname;
    
    UIImage *rightIrisSample;
    UIImage *rightIrisGray;
    UIImage *rightIrisRect;
    UIImage *rightIrisCode;
    
    UIImage *leftIrisSample;
    UIImage *leftIrisGray;
    UIImage *leftIrisRect;
    UIImage *leftIrisCode;
}

@property (weak, nonatomic) id <FURBEditViewControllerDelegate> delegate;
@property (strong, nonatomic) FURBPerson *person;

@property (strong, nonatomic) IBOutlet UITextField *firstTextField;
@property (strong, nonatomic) IBOutlet UITextField *surnameTextField;
@property (strong, nonatomic) IBOutlet UILabel *sexLabel;
@property (strong, nonatomic) IBOutlet UILabel *birthdateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *left;
@property (strong, nonatomic) IBOutlet UIImageView *right;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)update:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;

@end
