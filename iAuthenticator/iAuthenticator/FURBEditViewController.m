//
//  FURBEditViewController.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBEditViewController.h"
#import "FURBIrisCaptureViewController.h"
#import "FURBAppDelegate.h"
#import "FURBIrisCode.h"
#import "FURBUtil.h"

#define ACTIONSHEET_SEX     0
#define ACTIONSHEET_DATE    1

#define TAG_FIRST           0
#define TAG_SURNAME         1
#define TAG_SEX             2
#define TAG_BIRTHDATE       3
#define TAG_LEFTIRIS        4

@interface FURBEditViewController ()

@end

@implementation FURBEditViewController
@synthesize firstTextField;
@synthesize surnameTextField;
@synthesize sexLabel;
@synthesize birthdateLabel;
@synthesize left;
@synthesize right;
@synthesize activity;
@synthesize datePicker;
@synthesize person;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.firstTextField   setText:self.person.first];
    [self.surnameTextField setText:self.person.surname];
    
    first   = self.person.first;
    surname = self.person.surname; 
    
    NSString *sex;
    if ([self.person.sex boolValue]) {
        sex = @"Male";
    } else {
        sex = @"Female";
    }
    [self.sexLabel setText:sex];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd'/'MM'/'yyyy"];
	[self.birthdateLabel setText:[formatter stringFromDate:self.person.birthdate]];
    
    [self.left  setImage:[[UIImage alloc] initWithData:self.person.leftIrisSample]];
    [self.right setImage:[[UIImage alloc] initWithData:self.person.rightIrisSample]];
    
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.maximumDate = [NSDate date];
    self.datePicker.date = self.person.birthdate;
}

- (void)viewDidUnload
{
    [self setFirstTextField:nil];
    [self setSurnameTextField:nil];
    [self setSexLabel:nil];
    [self setBirthdateLabel:nil];
    [self setLeft:nil];
    [self setRight:nil];
    [self setDatePicker:nil];
    [self setActivity:nil];
    [self setDelegate:nil];
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"updateLeft"]) {
        
        FURBIrisCaptureViewController *irisCaptureViewController = segue.destinationViewController;
        irisCaptureViewController.temporaryImageView = left;
        
    } else if ([[segue identifier] isEqualToString:@"updateRight"]) {
        
        FURBIrisCaptureViewController *irisCaptureViewController = segue.destinationViewController;
        irisCaptureViewController.temporaryImageView = right;        
    }
}


- (IBAction)update:(id)sender
{
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    FURBAppDelegate *appDelegate = (FURBAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([firstTextField.text isEqualToString:@""]) {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The first name is required!" 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } 
    if ([surnameTextField.text isEqualToString:@""]) {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The surname is required!" 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    }
    
    // VERIFICAR SE O NOME FOI ALTERADO E, SE FOR, DEVE VERIFICAR SE JA
    // EXISTE NA BASE DE DADOS...
    if (![first   isEqualToString:[firstTextField text]] || 
        ![surname isEqualToString:[surnameTextField text]]) {
        
        id obj = [appDelegate loadFromName:[firstTextField text] :[surnameTextField text]];
        if (obj != nil) {
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"This person has been already registered!" 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating];
            
            [error show];
            return;
        }
    }
    
    if (left.image == nil || right.image == nil) {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"Both irises are required!" 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    }
    
    int  valuesR         [6];
    int  irisRectangleR  [256][100];
    BOOL irisMaskR       [256][100];
    BOOL bitCodeR        [2048];
    BOOL bitCodeMaskR    [2048];
    
    UIImage *rightTemp;
    
    rightIrisSample = right.image;
    
    rightIrisGray   = [[FURBUtil sharedInstance] grayscaleTransform:rightIrisSample];
    rightTemp       = rightIrisGray;
    rightIrisGray   = [[FURBUtil sharedInstance] applyMedianBlur:rightIrisGray];
    rightIrisGray   = [[FURBUtil sharedInstance] processHoughCircleTransform:rightIrisGray :valuesR]; 
    
    if (valuesR[3] == 0 || valuesR[4] == 0 || valuesR[5] == 0) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The right iris' circle could not be found! Please, take another photo of your right eye..." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } else {
        
        if (valuesR[0] == 0 || valuesR[1] == 0 || valuesR[2] == 0) {
            
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"The right pupil's circle could not be found! Please, take another photo of your right eye..." 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating]; 
            
            [error show];
            return;
        }
    }
    
    rightIrisRect = [[FURBUtil sharedInstance] normalize:rightTemp 
                                                        :valuesR 
                                                        :irisRectangleR
                                                        :irisMaskR];
    FURBIrisCode *codeR;
    codeR = [[FURBUtil sharedInstance] generateIrisCode:irisRectangleR
                                                       :irisMaskR
                                                       :bitCodeR
                                                       :bitCodeMaskR];
    rightIrisCode = [[FURBUtil sharedInstance] irisCodeRepresentation:bitCodeR :bitCodeMaskR];
    
    
    
    int  valuesL         [6];
    int  irisRectangleL  [256][100];
    BOOL irisMaskL       [256][100];
    BOOL bitCodeL        [2048];
    BOOL bitCodeMaskL    [2048];
    
    UIImage *leftTemp;
    
    leftIrisSample = left.image;
    leftIrisGray   = [[FURBUtil sharedInstance] grayscaleTransform:leftIrisSample];
    leftTemp       = leftIrisGray;
    leftIrisGray   = [[FURBUtil sharedInstance] applyMedianBlur:leftIrisGray];
    leftIrisGray   = [[FURBUtil sharedInstance] processHoughCircleTransform:leftIrisGray :valuesL]; 
    
    if (valuesL[3] == 0 || valuesL[4] == 0 || valuesL[5] == 0) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The left iris' circle could not be found! Please, take another photo of your left eye..." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } else {
        
        if (valuesL[0] == 0 || valuesL[1] == 0 || valuesL[2] == 0) {
            
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"The left pupil's circle could not be found! Please, take another photo of your left eye..." 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating];
            
            [error show];
            return;
        }
    }
    
    leftIrisRect = [[FURBUtil sharedInstance] normalize:leftTemp 
                                                       :valuesL 
                                                       :irisRectangleL
                                                       :irisMaskL];
    FURBIrisCode *codeL;
    codeL = [[FURBUtil sharedInstance] generateIrisCode:irisRectangleL
                                                       :irisMaskL
                                                       :bitCodeL
                                                       :bitCodeMaskL];
    leftIrisCode = [[FURBUtil sharedInstance] irisCodeRepresentation:bitCodeL :bitCodeMaskL];
    
    [self.person setFirst:[firstTextField text]];
    [self.person setSurname:[surnameTextField text]];
    [self.person setSex:[[NSNumber alloc] 
                         initWithBool:[sexLabel.text isEqualToString:@"Male"]]];
    [self.person setBirthdate:[datePicker date]];
    
    person.leftIrisSample       = UIImagePNGRepresentation(leftIrisSample);
    person.leftIrisGray         = UIImagePNGRepresentation(leftIrisGray);
    person.leftIrisRect         = UIImagePNGRepresentation(leftIrisRect);
    person.leftIrisCode         = UIImagePNGRepresentation(leftIrisCode);
    person.leftIrisCodeString   = [codeL toString];
    person.rightIrisSample      = UIImagePNGRepresentation(rightIrisSample);
    person.rightIrisGray        = UIImagePNGRepresentation(rightIrisGray);
    person.rightIrisRect        = UIImagePNGRepresentation(rightIrisRect);
    person.rightIrisCode        = UIImagePNGRepresentation(rightIrisCode); 
    person.rightIrisCodeString  = [codeR toString];
    
    
    [appDelegate.managedObjectContext save:nil];
    
    [activity stopAnimating];
    [self.delegate buttonUpdateInEditViewWasPressed:self];
}

- (IBAction)textFieldDoneEditing:(id)sender { 
    
    [sender resignFirstResponder];
}







#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    NSInteger tag = [cell tag];
    
    UIActionSheet *actionSheet;
    
    switch (tag) {
        case TAG_SEX:
            
            actionSheet = [[UIActionSheet alloc] 
                           initWithTitle:nil
                           delegate:self
                           cancelButtonTitle:@"Cancel" 
                           destructiveButtonTitle:nil 
                           otherButtonTitles:@"Female", @"Male", nil];
            
            [actionSheet showInView:self.tabBarController.tabBar];
            [actionSheet setTag:ACTIONSHEET_SEX];
            
            break;
            
        case TAG_BIRTHDATE:
        {    
            NSString *cancelButton = nil;
            
            if ([[UIDevice currentDevice] orientation] == UIInterfaceOrientationPortrait) {
                cancelButton = @"Cancel";
            }
            
            actionSheet = [[UIActionSheet alloc] 
                           initWithTitle:nil 
                           delegate:self
                           cancelButtonTitle:cancelButton
                           destructiveButtonTitle:nil
                           otherButtonTitles:@"Select",nil];    
            
            [actionSheet showInView:self.tabBarController.tabBar];
            [actionSheet addSubview:self.datePicker];
            
            
            CGRect menuRect = actionSheet.frame;
            
            CGFloat orgHeight = menuRect.size.height;
            menuRect.origin.y -= 214;
            menuRect.size.height = orgHeight+214;
            actionSheet.frame = menuRect;
            
            CGRect pickerRect = self.datePicker.frame;
            pickerRect.origin.y = orgHeight;
            
            // linha de baixo adicionada para que o
            // date picker seja corretamente dimensionado
            // em modo paisagem!
            pickerRect.size.width = menuRect.size.width;
            self.datePicker.frame = pickerRect;    
            
            [actionSheet setTag:ACTIONSHEET_DATE];
            
            break;
        }   
            
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    switch (actionSheet.tag) {
        case ACTIONSHEET_SEX:
            
            if (buttonIndex == 0) {
                // Female was selected
                
                if ([[self.sexLabel text] isEqualToString:@"Male"]) {
                    // Actual sex is Male
                    
                    self.sexLabel.text = @"Female";
                }
                
            } else if (buttonIndex == 1) {
                // Male was selected
                
                if ([[self.sexLabel text] isEqualToString:@"Female"]) {
                    // Actual sex is Female
                    
                    self.sexLabel.text = @"Male";
                } 
            }   
            
            break;
            
        case ACTIONSHEET_DATE:
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd'/'MM'/'yyyy"];
            
            if (buttonIndex == 0) {
                [self.birthdateLabel setText:[formatter stringFromDate:[datePicker date]]];
            } else {
                [self.datePicker setDate:[formatter dateFromString:self.birthdateLabel.text]];
            }
            
            break;
        }
        default:
            break;
    }
}

- (void) threadStartAnimating:(id)data {
    [activity startAnimating];
}

@end
