//
//  FURBIdentificationViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FURBIdentificationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *myImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (IBAction)clear:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (IBAction)identify:(id)sender;
@end
