//
//  FURBIdentificationViewController.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBIdentificationViewController.h"
#import "FURBIrisCaptureViewController.h"
#import "FURBPerson.h"
#import "FURBUtil.h"

@interface FURBIdentificationViewController ()

@end

@implementation FURBIdentificationViewController
@synthesize myImageView;
@synthesize activity;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setMyImageView:nil];
    [self setActivity:nil];
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"captureToIdentify"]) {
        
        FURBIrisCaptureViewController *irisCaptureViewController = segue.destinationViewController;
        irisCaptureViewController.temporaryImageView = myImageView;
    } 
}

- (IBAction)clear:(id)sender {
    
    [myImageView setImage:[UIImage imageNamed:@"eye.png"]];
}

- (IBAction)takePhoto:(id)sender {
}

- (IBAction)identify:(id)sender {
    
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    
    int  values         [6];
    int  irisRectangle  [256][100];
    BOOL irisMask       [256][100];
    BOOL bitCode        [2048];
    BOOL bitCodeMask    [2048];
    
    UIImage      *irisSample;
    UIImage      *irisGray;
    UIImage      *irisRect;
    UIImage      *irisTemp;
    FURBIrisCode *irisCode;
    
    irisSample = myImageView.image;
    irisGray   = [[FURBUtil sharedInstance] grayscaleTransform:irisSample];
    irisTemp   = irisGray;
    irisGray   = [[FURBUtil sharedInstance] applyMedianBlur:irisGray];
    irisGray   = [[FURBUtil sharedInstance] processHoughCircleTransform:irisGray :values]; 
    
    if (values[3] == 0 || values[4] == 0 || values[5] == 0) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The iris' circle could not be found! Please, take another photo of your eye..." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } else {
        
        if (values[0] == 0 || values[1] == 0 || values[2] == 0) {
            
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"The pupil's circle could not be found! Please, take another photo of your eye..." 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating];
            
            [error show];
            return;
        }
    }
    
    irisRect = [[FURBUtil sharedInstance] normalize:irisTemp 
                                                   :values
                                                   :irisRectangle
                                                   :irisMask];
    
    irisCode = [[FURBUtil sharedInstance] generateIrisCode:irisRectangle
                                                          :irisMask
                                                          :bitCode
                                                          :bitCodeMask];
    
    FURBPerson *p = [[FURBUtil sharedInstance] identifyEye
                    :bitCode
                    :bitCodeMask];
    
    [activity stopAnimating];
    
    if (p == nil) {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Warn" 
                              message:@"No person was found in the iAuthenticator's DataBase that matches with the iris you've provided." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
    } else {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Info" 
                              message:[[NSString alloc] 
                                       initWithFormat:
                                       @"The iris you've provided matches with one of %@ %@'s irises.", 
                                       [p first], 
                                       [p surname]]
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
    }
}

- (void) threadStartAnimating:(id)data {
    [activity startAnimating];
}

@end
