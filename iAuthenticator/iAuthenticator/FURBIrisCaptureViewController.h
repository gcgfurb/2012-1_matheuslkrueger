//
//  FURBIrisCaptureViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface FURBIrisCaptureViewController : UIViewController
{
    AVCaptureSession *session;
    AVCaptureStillImageOutput *stillImageOutput;
    AVCaptureVideoPreviewLayer *previewLayer;
    UIImage *myImage;
}

@property (strong, nonatomic) IBOutlet UIView *previewView;
@property (strong, nonatomic) IBOutlet UIToolbar *myToolbar;
@property (strong, nonatomic) IBOutlet UIImageView *managedImageView;
@property (strong, nonatomic) IBOutlet UIImageView *temporaryImageView;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *takeButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *useButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *discardButton;

- (IBAction)take:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)use:(id)sender;
- (IBAction)discard:(id)sender;

@end
