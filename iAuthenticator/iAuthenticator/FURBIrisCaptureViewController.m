//
//  FURBIrisCaptureViewController.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBIrisCaptureViewController.h"
#import <ImageIO/ImageIO.h>
#import "UIImage+Crop.h"
#import "FURBUtil.h"

@interface FURBIrisCaptureViewController ()
- (void)image:(UIImage *)image 
    didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
@end

@implementation FURBIrisCaptureViewController
@synthesize previewView;
@synthesize myToolbar;
@synthesize managedImageView;
@synthesize useButton;
@synthesize discardButton;
@synthesize takeButton;
@synthesize cancelButton;
@synthesize temporaryImageView;

- (BOOL)setupAVCapture
{
    NSError *error = nil;
    
    session = [AVCaptureSession new];
	[session setSessionPreset:AVCaptureSessionPresetPhoto];
	
	// Select a video device, make an input
	AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    
    // configuration (focus, exposure, flash)
    if ([backCamera lockForConfiguration:&error]) {
       
        CGPoint autofocusPoint = CGPointMake(0.5f, 0.5f);
        [backCamera setFocusPointOfInterest:autofocusPoint];
        [backCamera setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        
        CGPoint exposurePoint = CGPointMake(0.5f, 0.5f);
        [backCamera setExposurePointOfInterest:exposurePoint];
        [backCamera setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        
        backCamera.flashMode = AVCaptureFlashModeOn;    

        [backCamera unlockForConfiguration];
    }
    // end configuration
    
    
	AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:backCamera 
                                                        error:&error];
	if (error)
		return NO;
	if ([session canAddInput:input])
		[session addInput:input];
	
	// Make a still image output
	stillImageOutput = [AVCaptureStillImageOutput new];
    NSDictionary *outputSettings = [[NSDictionary alloc] 
                                    initWithObjectsAndKeys:AVVideoCodecJPEG,
                                    AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
	if ([session canAddOutput:stillImageOutput])
		[session addOutput:stillImageOutput];
	
	// Make a preview layer so we can see the visual output of an AVCaptureSession
    previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];   
	[previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
	[previewLayer setFrame:[previewView bounds]];
    [previewLayer setOrientation:AVCaptureVideoOrientationLandscapeRight];
	
    // add the preview layer to the hierarchy
    CALayer *rootLayer = [previewView layer];
	[rootLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
	[rootLayer addSublayer:previewLayer];
	
    [session startRunning];
	
	return YES;
}

- (IBAction)take:(id)sender 
{
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break; 
            }
        }
        if (videoConnection) { 
            break; 
        }
    }
    
    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:
     ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
         
         CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer,kCGImagePropertyExifDictionary, NULL);
         
         if (exifAttachments) {
             
             NSData *imageData = [AVCaptureStillImageOutput 
                                  jpegStillImageNSDataRepresentation:imageSampleBuffer];
             UIImage *image    = [[UIImage alloc] initWithData:imageData];
             
             image = [[FURBUtil sharedInstance] cropImage:image];
             
             [managedImageView setImage:image];
             
             [useButton      setEnabled:YES];
             [discardButton  setEnabled:YES];
             [takeButton     setEnabled:NO];
             [cancelButton   setEnabled:NO];
             
         } else {
             // Do other thing.
         }
     }
     ];
}

- (IBAction)cancel:(id)sender 
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)use:(id)sender 
{
    [temporaryImageView setImage:managedImageView.image];

    [self cancel:nil];
}

- (IBAction)discard:(id)sender {
    
    [managedImageView setImage:nil];
    
    [useButton      setEnabled:NO];
    [discardButton  setEnabled:NO];

    [takeButton     setEnabled:YES];
    [cancelButton   setEnabled:YES];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

# pragma mark - View lifecycle

- (void)viewDidLoad
{
    [self setupAVCapture];
    
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setPreviewView:nil];
    [self setMyToolbar:nil];
    [self setManagedImageView:nil];
    [self setUseButton:nil];
    [self setDiscardButton:nil];
    [self setTakeButton:nil];
    [self setCancelButton:nil];
    [self setTemporaryImageView:nil];
    
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
