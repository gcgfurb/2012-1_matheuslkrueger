//
//  FURBIrisCode.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef unsigned long int Bitcode32;

@interface FURBIrisCode : NSObject
{
    Bitcode32 code[64];
    Bitcode32 mask[64];
}

- (id)initWithString :(NSString *)codeandmask;
- (NSString *)toString;

/*!
 * Constuctor which constructs iris codes from the database.
 *
 * @param _code Iris bitcode as an array of 64 32-bit integers.
 * @param _mask Iris bitmask as an array of 64 32-bit integers.
 */
- (id)initWithBool :(BOOL[2048])_code :(BOOL[2048])_mask :(int)shift;

/*!
 * Constuctor which constructs iris codes from the database.
 *
 * @param _code Iris bitcode as an array of 64 32-bit integers.
 * @param _mask Iris bitmask as an array of 64 32-bit integers.
 */
- (id)initWithBitcode :(Bitcode32[64])_code :(Bitcode32[64])_mask;

/*!
 * Function which compares two iris codes utilizing the full potential of a 32-bit architecture. 
 *
 * @param other Iris code to compare with.
 * @return Hamming distance between the two.
 */
- (double)compare :(FURBIrisCode *)other;

/*!
 * Get a 32 bit segment of the iris bitcode
 *
 * @param i Number of segment.
 * @return A 32 bit bitcode.
 */
- (Bitcode32)getCode :(int)i;

/*!
 * Get a 32 bit segment of the iris bitmask
 *
 * @param i Number of segment.
 * @return A 32 bit bitmask.
 */
- (Bitcode32)getMask :(int)i;

- (void)print;

@end
