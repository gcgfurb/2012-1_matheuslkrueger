//
//  FURBIrisCode.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBIrisCode.h"

@implementation FURBIrisCode

- (id)initWithString :(NSString *)codeandmask
{
    NSArray *list = [codeandmask componentsSeparatedByString:@", "];
    
    for (int i = 0, j = 0; i < [list count]; i += 2, j++) {
        
        code[j] = [[list objectAtIndex:i+0] longLongValue];
        mask[j] = [[list objectAtIndex:i+1] longLongValue];
    }
    
    return self;
}

- (NSString *)toString
{
    NSMutableString *codeandmask = [[NSMutableString alloc] init];
    
    for (int i = 0; i < 64; i++) {
        
        [codeandmask appendString:[[NSString alloc] initWithFormat:@"%lu, ", code[i]]];
        
        if (i != 63) {
            [codeandmask appendString:[[NSString alloc] initWithFormat:@"%lu, ", mask[i]]];
        } else {
            [codeandmask appendString:[[NSString alloc] initWithFormat:@"%lu" , mask[i]]];
        }
    }
    
    return [NSString stringWithString:codeandmask];
}

- (id)initWithBool :(BOOL[2048])_code :(BOOL[2048])_mask :(int)shift
{
    while (shift < 0 || 2047 < shift) {
		if (shift < 0)
			shift += 2048;
		if (shift > 2047)
			shift -= 2048;
	}
    
	for (int i = 0; i < 64; i++){
		code[i] = 0;
		mask[i] = 0;
	}
    
	for (int i = 0; i < 2048; i++){
        if (_code[i] == YES)
            code[((i + shift) / 32) % 64] += (Bitcode32) (pow(2.0,(31 - (i + shift) % 32)));
        if (_mask[i] == YES)
            mask[((i + shift) / 32) % 64] += (Bitcode32) (pow(2.0,(31 - (i + shift) % 32)));
	}
    
    return self;
}

- (id)initWithBitcode :(Bitcode32[64])_code :(Bitcode32[64])_mask
{
    for (int i = 0; i < 64; i++) {
        
        code[i] = _code[i];
        mask[i] = _mask[i];
	}
    
    return self;
}

- (double)compare :(FURBIrisCode *)other
{
    Bitcode32 temp[64];
	int top=0;
	int bottom=0;
    
	for (int i = 0; i < 64; i++)
		temp[i] = code[i] ^ [other getCode:i];
    
	for (int i = 0; i < 64; i++)
		temp[i] = temp[i] & mask[i];
    
	for (int i = 0; i < 64; i++)
		temp[i] = temp[i] & [other getMask:i];
    
	for (int i = 0; i < 64; i++){
	    for (int j = 0; j < 32; j++){
    		if (temp[i] & 0x01)
    			++top;
            temp[i] >>= 1;
    	}
	}
    
	for (int i = 0; i < 64; i++)
		temp[i] = mask[i] & [other getMask:i];
	
	for (int i = 0; i < 64; i++){
        for (int j = 0; j < 32; j++){
    		if (temp[i] & 0x01)
	    		++bottom;
            temp[i] >>= 1;
	    }
	}
	
	/* In the case (included in test2.cpp) where number of useful bits is very low (192 in this case), top can equal zero. Due to the way the HD is calculated this gives a false positive. The following deals with cases of top = 0 or bottom =0 
     
     We note that we may wish to form a better formula, talk to Duncan about this */
	
	/* debugging */
	//cout << "\nTop = " << top << endl;
	//cout << "Bottom = " << bottom << endl << endl;
    
    if(bottom == 0){
        //printf("ERROR! NUMBER USEFUL BITS WAS ZERO!");
        return 1;
    }
    
    if(top == 0){
        //printf("NUMBER OF DIFFERENCES WERE ZERO!");
        return ((double)(2048) - bottom) / 2048;
    }	 
    
	return (double)(top) / bottom;
}

- (Bitcode32)getCode :(int)i
{
    return code[i];
}

- (Bitcode32)getMask :(int)i
{
    return mask[i];
}

- (void)print
{

}

@end
