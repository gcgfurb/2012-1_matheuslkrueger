//
//  FURBNewViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FURBNewViewController;
@protocol FURBNewViewControllerDelegate
- (void)buttonSaveInNewPersonWasPressed :(FURBNewViewController *)controller;
@end

@interface FURBNewViewController : UITableViewController
    <UIActionSheetDelegate>
{
    UIImage *rightIrisSample;
    UIImage *rightIrisGray;
    UIImage *rightIrisRect;
    UIImage *rightIrisCode;
    
    UIImage *leftIrisSample;
    UIImage *leftIrisGray;
    UIImage *leftIrisRect;
    UIImage *leftIrisCode;
}

@property (weak, nonatomic) id <FURBNewViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITextField *firstTextField;
@property (strong, nonatomic) IBOutlet UITextField *surnameTextField;
@property (strong, nonatomic) IBOutlet UILabel *sexLabel;
@property (strong, nonatomic) IBOutlet UILabel *birthdateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *left;
@property (strong, nonatomic) IBOutlet UIImageView *right;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)save:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;

@end