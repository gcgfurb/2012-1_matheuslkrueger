//
//  FURBNewViewController.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBNewViewController.h"
#import "FURBAppDelegate.h"
#import "FURBPerson.h"
#import "FURBIrisCaptureViewController.h"
#import "FURBUtil.h"
#import "FURBIrisCode.h"
#import <mach/mach_time.h>

#define ACTIONSHEET_SEX     0
#define ACTIONSHEET_DATE    1

#define TAG_FIRST           0
#define TAG_SURNAME         1
#define TAG_SEX             2
#define TAG_BIRTHDATE       3
#define TAG_LEFTIRIS        4

@interface FURBNewViewController ()

@end

@implementation FURBNewViewController

@synthesize delegate;

@synthesize firstTextField;
@synthesize surnameTextField;
@synthesize sexLabel;
@synthesize birthdateLabel;
@synthesize left;
@synthesize right;
@synthesize activity;
@synthesize datePicker;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.maximumDate = [NSDate date];
    
    NSDate *now = [NSDate date];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd'/'MM'/'yyyy"];
	[self.birthdateLabel setText:[formatter stringFromDate:now]];
    
    [self.sexLabel setText:@"Male"];
}

- (void)viewDidUnload
{
    [self setDelegate:nil];                 // Será?
    [self setFirstTextField:nil];
    [self setSurnameTextField:nil];
    [self setSexLabel:nil];
    [self setBirthdateLabel:nil];
    [self setLeft:nil];
    [self setRight:nil];
    [self setDatePicker:nil];
    
    [self setActivity:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"captureLeft"]) {
        
        FURBIrisCaptureViewController *irisCaptureViewController = segue.destinationViewController;
        irisCaptureViewController.temporaryImageView = left;
        
    } else if ([[segue identifier] isEqualToString:@"captureRight"]) {
        
        FURBIrisCaptureViewController *irisCaptureViewController = segue.destinationViewController;
        irisCaptureViewController.temporaryImageView = right;
        
    }
}

- (IBAction)save:(id)sender
{
    
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    
    FURBAppDelegate *appDelegate = (FURBAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if ([firstTextField.text isEqualToString:@""]) {
        UIAlertView *error = [[UIAlertView alloc]
                       initWithTitle:@"Error" 
                       message:@"The first name is required!" 
                       delegate:self 
                       cancelButtonTitle:@"Ok" 
                       otherButtonTitles:nil];

        [activity stopAnimating];

        [error show];
        return;
    } 
    if ([surnameTextField.text isEqualToString:@""]) {
        UIAlertView *error = [[UIAlertView alloc]
                      initWithTitle:@"Error" 
                      message:@"The surname is required!" 
                      delegate:self 
                      cancelButtonTitle:@"Ok" 
                      otherButtonTitles:nil];

        [activity stopAnimating];

        [error show];
        return;
    }
    
    // testar nome igual ja cadastrado na base...
    id obj = [appDelegate loadFromName:[firstTextField text] :[surnameTextField text]];
    if (obj != nil) {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"This person has been already registered!" 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    }
    
    
    if (left.image == nil || right.image == nil) {
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"Both irises are required!" 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    }
    
    int  valuesR         [6];
    int  irisRectangleR  [256][100];
    BOOL irisMaskR       [256][100];
    BOOL bitCodeR        [2048];
    BOOL bitCodeMaskR    [2048];
    
    UIImage *rightTemp;
    
    rightIrisSample = right.image;
    rightIrisGray   = [[FURBUtil sharedInstance] grayscaleTransform:rightIrisSample];
    rightTemp       = rightIrisGray;
    rightIrisGray   = [[FURBUtil sharedInstance] applyMedianBlur:rightIrisGray];
    rightIrisGray   = [[FURBUtil sharedInstance] processHoughCircleTransform:rightIrisGray :valuesR]; 
    
    if (valuesR[3] == 0 || valuesR[4] == 0 || valuesR[5] == 0) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The right iris' circle could not be found! Please, take another photo of your right eye..." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } else {
        
        if (valuesR[0] == 0 || valuesR[1] == 0 || valuesR[2] == 0) {
            
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"The right pupil's circle could not be found! Please, take another photo of your right eye..." 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating]; 
            
            [error show];
            return;
        }
    }
    
    rightIrisRect = [[FURBUtil sharedInstance] normalize:rightTemp 
                                                         :valuesR 
                                                         :irisRectangleR
                                                         :irisMaskR];
    FURBIrisCode *codeR;
    codeR = [[FURBUtil sharedInstance] generateIrisCode:irisRectangleR
                                                       :irisMaskR
                                                       :bitCodeR
                                                       :bitCodeMaskR];
    rightIrisCode = [[FURBUtil sharedInstance] irisCodeRepresentation:bitCodeR :bitCodeMaskR];
    
    
    
    int  valuesL         [6];
    int  irisRectangleL  [256][100];
    BOOL irisMaskL       [256][100];
    BOOL bitCodeL        [2048];
    BOOL bitCodeMaskL    [2048];
    
    UIImage *leftTemp;
    
    leftIrisSample = left.image;
    leftIrisGray   = [[FURBUtil sharedInstance] grayscaleTransform:leftIrisSample];
    leftTemp       = leftIrisGray;
    leftIrisGray   = [[FURBUtil sharedInstance] applyMedianBlur:leftIrisGray];
    leftIrisGray   = [[FURBUtil sharedInstance] processHoughCircleTransform:leftIrisGray :valuesL]; 
    
    if (valuesL[3] == 0 || valuesL[4] == 0 || valuesL[5] == 0) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The left iris' circle could not be found! Please, take another photo of your left eye..." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } else {
        
        if (valuesL[0] == 0 || valuesL[1] == 0 || valuesL[2] == 0) {
            
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"The left pupil's circle could not be found! Please, take another photo of your left eye..." 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating];
            
            [error show];
            return;
        }
    }
    
    leftIrisRect = [[FURBUtil sharedInstance] normalize:leftTemp 
                                                        :valuesL 
                                                        :irisRectangleL
                                                        :irisMaskL];
    FURBIrisCode *codeL;
    codeL = [[FURBUtil sharedInstance] generateIrisCode:irisRectangleL
                                                       :irisMaskL
                                                       :bitCodeL
                                                       :bitCodeMaskL];
    leftIrisCode = [[FURBUtil sharedInstance] irisCodeRepresentation:bitCodeL :bitCodeMaskL];
    
    
    
    
    FURBPerson *person = [NSEntityDescription 
                          insertNewObjectForEntityForName:@"FURBPerson" 
                          inManagedObjectContext:appDelegate.managedObjectContext];
    
    person.first = firstTextField.text;
    person.surname = surnameTextField.text;
    person.sex = [[NSNumber alloc] 
                  initWithBool:[sexLabel.text isEqualToString:@"Male"]];
    person.birthdate = datePicker.date;
    
    person.leftIrisSample       = UIImagePNGRepresentation(leftIrisSample);
    person.leftIrisGray         = UIImagePNGRepresentation(leftIrisGray);
    person.leftIrisRect         = UIImagePNGRepresentation(leftIrisRect);
    person.leftIrisCode         = UIImagePNGRepresentation(leftIrisCode);
    person.leftIrisCodeString   = [codeL toString];
    
    person.rightIrisSample      = UIImagePNGRepresentation(rightIrisSample);
    person.rightIrisGray        = UIImagePNGRepresentation(rightIrisGray);
    person.rightIrisRect        = UIImagePNGRepresentation(rightIrisRect);
    person.rightIrisCode        = UIImagePNGRepresentation(rightIrisCode); 
    person.rightIrisCodeString  = [codeR toString];
    
    [appDelegate.managedObjectContext save:nil];
    [self.delegate buttonSaveInNewPersonWasPressed:self];
    
    [activity stopAnimating];
}

- (IBAction)textFieldDoneEditing:(id)sender { 
    
    [sender resignFirstResponder];
}







#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    NSInteger tag = [cell tag];
    
    UIActionSheet *actionSheet;
    
    switch (tag) {
        case TAG_SEX:
            
            actionSheet = [[UIActionSheet alloc] 
                           initWithTitle:nil
                           delegate:self
                           cancelButtonTitle:@"Cancel" 
                           destructiveButtonTitle:nil 
                           otherButtonTitles:@"Female", @"Male", nil];
            
            [actionSheet showInView:self.tabBarController.tabBar];
            [actionSheet setTag:ACTIONSHEET_SEX];
            
            break;
            
        case TAG_BIRTHDATE:
        {    
            NSString *cancelButton = nil;
            
            if ([[UIDevice currentDevice] orientation] == UIInterfaceOrientationPortrait) {
                cancelButton = @"Cancel";
            }
            
            actionSheet = [[UIActionSheet alloc] 
                           initWithTitle:nil 
                           delegate:self
                           cancelButtonTitle:cancelButton
                           destructiveButtonTitle:nil
                           otherButtonTitles:@"Select",nil];    
            
            [actionSheet showInView:self.tabBarController.tabBar];
            [actionSheet addSubview:self.datePicker];

            
            CGRect menuRect = actionSheet.frame;
            
            CGFloat orgHeight = menuRect.size.height;
            menuRect.origin.y -= 214;
            menuRect.size.height = orgHeight+214;
            actionSheet.frame = menuRect;
            
            CGRect pickerRect = self.datePicker.frame;
            pickerRect.origin.y = orgHeight;

            // linha de baixo adicionada para que o
            // date picker seja corretamente dimensionado
            // em modo paisagem!
            pickerRect.size.width = menuRect.size.width;
            self.datePicker.frame = pickerRect;    
            
            [actionSheet setTag:ACTIONSHEET_DATE];
            
            break;
        }   
        case TAG_LEFTIRIS:
            
            // fazer ir para o xib de captura...
            //FURBIrisCaptureViewController *captureController = [[FURBIrisCaptureViewController al
            
            break;
            
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    // Arrumar essa gambi...
    // existe uma forma mais complicada, porem mais correta de se fazer isso
    // dessa forma fica grande a view, buga qndo muda a orientacao
    // por enquanto ok, mas tem que mudar!
    /*
     UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil 
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"OK",nil];    
     
     [actionSheet addSubview:self.datePicker];
     [actionSheet showInView:self.view];        
     
     CGRect menuRect = actionSheet.frame;
     CGFloat orgHeight = menuRect.size.height;
     menuRect.origin.y -= 214; //height of picker
     menuRect.size.height = orgHeight+214;
     actionSheet.frame = menuRect;
     
     
     CGRect pickerRect = self.datePicker.frame;
     pickerRect.origin.y = orgHeight;
     self.datePicker.frame = pickerRect;    
     
     actionSheet.tag = ACTIONSHEET_DATE;
     */
    // http://stackoverflow.com/questions/1262574/add-uipickerview-a-button-in-action-sheet-how
    // http://stackoverflow.com/questions/349858/fitting-a-uidatepicker-into-a-uiactionsheet
    
    /*
     UIActionSheet *aac = [[UIActionSheet alloc] 
     initWithTitle:@"How many?"
     delegate:self
     cancelButtonTitle:nil
     destructiveButtonTitle:nil
     otherButtonTitles:nil];
     
     UIDatePicker *theDatePicker = [[UIDatePicker alloc] 
     initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
     
     theDatePicker.datePickerMode = UIDatePickerModeDate;
     theDatePicker.maximumDate=[NSDate date];
     
     
     self.datePicker = theDatePicker;
     
     UIToolbar *pickerDateToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
     pickerDateToolbar.barStyle = UIBarStyleBlackOpaque;
     [pickerDateToolbar sizeToFit];
     
     NSMutableArray *barItems = [[NSMutableArray alloc] init];
     
     UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
     [barItems addObject:flexSpace];
     
     UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DatePickerDoneClick:)];
     [barItems addObject:doneBtn];
     
     [pickerDateToolbar setItems:barItems animated:YES];
     
     [aac addSubview:pickerDateToolbar];
     [aac addSubview:self.datePicker];
     [aac showInView:self.view];
     [aac setBounds:CGRectMake(0,0,320, 464)];
     */
}


#pragma mark - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    switch (actionSheet.tag) {
        case ACTIONSHEET_SEX:
            
            if (buttonIndex == 0) {
                // Female was selected
                
                if ([[self.sexLabel text] isEqualToString:@"Male"]) {
                    // Actual sex is Male
                    
                    self.sexLabel.text = @"Female";
                }
                
            } else if (buttonIndex == 1) {
                // Male was selected
                
                if ([[self.sexLabel text] isEqualToString:@"Female"]) {
                    // Actual sex is Female
                    
                    self.sexLabel.text = @"Male";
                } 
            }   
            
            break;
            
        case ACTIONSHEET_DATE:
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd'/'MM'/'yyyy"];
            
            if (buttonIndex == 0) {
                [self.birthdateLabel setText:[formatter stringFromDate:[datePicker date]]];
            } else {
                [self.datePicker setDate:[formatter dateFromString:self.birthdateLabel.text]];
            }
             
            break;
        }
        default:
            break;
    }
}

- (void) threadStartAnimating:(id)data {
    [activity startAnimating];
}

double MachTimeToSecs(uint64_t time)
{
    mach_timebase_info_data_t timebase;
    mach_timebase_info(&timebase);
    return (double)time * (double)timebase.numer / (double)timebase.denom / 1e9;
}

@end
