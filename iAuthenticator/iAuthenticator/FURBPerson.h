//
//  FURBPerson.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FURBPerson : NSManagedObject

@property (nonatomic, retain) NSString * first;
@property (nonatomic, retain) NSString * surname;
@property (nonatomic, retain) NSNumber * sex;
@property (nonatomic, retain) NSDate   * birthdate;

@property (nonatomic, retain) NSData   * leftIrisSample;
@property (nonatomic, retain) NSData   * leftIrisGray;
@property (nonatomic, retain) NSData   * leftIrisRect;
@property (nonatomic, retain) NSData   * leftIrisCode;
@property (nonatomic, retain) NSString * leftIrisCodeString;

@property (nonatomic, retain) NSData   * rightIrisSample;
@property (nonatomic, retain) NSData   * rightIrisGray;
@property (nonatomic, retain) NSData   * rightIrisRect;
@property (nonatomic, retain) NSData   * rightIrisCode;
@property (nonatomic, retain) NSString * rightIrisCodeString;

@end
