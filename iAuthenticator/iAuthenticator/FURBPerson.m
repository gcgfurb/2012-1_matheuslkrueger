//
//  FURBPerson.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBPerson.h"

@implementation FURBPerson

@dynamic first;
@dynamic surname;
@dynamic birthdate;
@dynamic sex;

@dynamic leftIrisSample;
@dynamic leftIrisGray;
@dynamic leftIrisRect;
@dynamic leftIrisCode;
@dynamic leftIrisCodeString;

@dynamic rightIrisSample;
@dynamic rightIrisGray;
@dynamic rightIrisRect;
@dynamic rightIrisCode;
@dynamic rightIrisCodeString;

@end
