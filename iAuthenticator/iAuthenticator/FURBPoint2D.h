//
//  FURBPoint2D.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FURBPoint2D : NSObject
{
    int x;
    int y;
}

- (id)init;

- (int)x;
- (void)setX :(int)newX;

- (int)y;
- (void)setY :(int)newY;

@end
