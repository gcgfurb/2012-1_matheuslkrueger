//
//  FURBPoint2D.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBPoint2D.h"

@implementation FURBPoint2D

- (id)init
{
    x = 0;
    y = 0;
    
    return self;
}

- (int)x
{
    return x;
}

- (void)setX :(int)newX
{
    x = newX;
}

- (int)y
{
    return y;
}

- (void)setY :(int)newY
{
    y = newY;
}

@end
