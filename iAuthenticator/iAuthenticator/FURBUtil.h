//
//  FURBUtil.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Accelerate/Accelerate.h>
#import "FURBIrisCode.h"
#import "FURBPerson.h"

@interface FURBUtil : NSObject

+ (id)sharedInstance;

- (UIImage *)cropImage                      :(UIImage *)src;
- (UIImage *)grayscaleTransform             :(UIImage *)src;
- (UIImage *)applyMedianBlur                :(UIImage *)src;
- (UIImage *)processHoughCircleTransform    :(UIImage *)src     
                                            :(int[6])values;

- (UIImage *)normalize  :(UIImage *)src 
                        :(int [6])values
                        :(int [256][100])irisRectangle 
                        :(BOOL[256][100])irisMask;

- (UIImage *)irisCodeRepresentation :(BOOL[2048])bitCode
                                    :(BOOL[2048])bitCodeMask;

- (FURBIrisCode *)generateIrisCode :(int [256][100])irisRectangle 
                                   :(BOOL[256][100])irisMask
                                   :(BOOL[2048])bitCode
                                   :(BOOL[2048])bitCodeMask;

- (BOOL) compareToVerify    :(NSString *)leftIrisCodeString
                            :(NSString *)rightIrisCodeString
                            :(BOOL[2048])bitCode
                            :(BOOL[2048])bitCodeMask;

- (FURBPerson *) identifyEye    :(BOOL[2048])bitCode
                                :(BOOL[2048])bitCodeMask;

- (double) getMinHD :(NSString *)firstIrisString
                    :(BOOL[2048])bitCode
                    :(BOOL[2048])bitCodeMask;
@end
