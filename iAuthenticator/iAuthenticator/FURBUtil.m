//
//  FURBUtil.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBUtil.h"
#import "FURBAppDelegate.h"
#import "UIImage+Crop.h"
#import "FURBPoint2D.h"

@interface FURBUtil ()
- (UIImage *)createImageFromPlanar8:(const vImage_Buffer)imageBuffer;
- (int)getPixel :(Pixel_8 *)bitmap :(int)width :(int)height :(int)row :(int)col;
- (long)getPosition :(int)width :(int)height :(int)row :(int)col;
- (int) medianH :(int *)kernelHistogram;
- (int)pupilThreshold :(UIImage *)img;
- (vImagePixelCount *)buildHistogram :(UIImage *)img;
@end

@implementation FURBUtil

+ (id)sharedInstance
{
    // singleton
    // http://lukeredpath.co.uk/blog/a-note-on-objective-c-singletons.html
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (UIImage *)cropImage :(UIImage *)src
{
    CGFloat w3 = roundf(src.size.width/3);
    CGFloat h3 = roundf(src.size.height/3);
 
    CGRect rect = CGRectMake(w3, h3, w3, h3);
    return [src crop:rect];
}

- (UIImage *)grayscaleTransform :(UIImage *)src
{
    CGRect imageRect = CGRectMake(0, 
                                  0, 
                                  src.size.width, 
                                  src.size.height);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, 
                                                 src.size.width, 
                                                 src.size.height, 
                                                 8, 
                                                 0, 
                                                 colorSpace, 
                                                 kCGImageAlphaNone);
    
    CGContextDrawImage(context, imageRect, [src CGImage]);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    UIImage *dst = [UIImage imageWithCGImage:imageRef];
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    return dst;
}

- (UIImage *)applyMedianBlur :(UIImage *)src
{
    CGImageRef imageCGSource = [src CGImage];
    
    size_t width  = CGImageGetWidth(imageCGSource);
    size_t height = CGImageGetHeight(imageCGSource);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();

    Pixel_8 *bitmap = (Pixel_8 *)malloc(width * height * sizeof(Pixel_8));
    for (int i = 0 ; i < (width * height * sizeof(Pixel_8)); i++) {
        bitmap[i] = 0;
    }
    
    long bytesPerPixel = 1;
    long bytesPerRow = bytesPerPixel * width;
    long bitsPerComponent = 8;
    
    CGContextRef context = CGBitmapContextCreate(bitmap, 
                                                 width, 
                                                 height, 
                                                 bitsPerComponent, 
                                                 bytesPerRow, 
                                                 colorSpace, 
                                                 kCGImageAlphaNone);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageCGSource);
    
    // my dest bitmap
    Pixel_8 *dest_bitmap = (Pixel_8 *)malloc(width * height * sizeof(Pixel_8));
    for (int i = 0 ; i < (width * height * sizeof(Pixel_8)); i++) {
        dest_bitmap[i] = 0;
    }
    
    // Median Blur Implementation
    int kernel = 9;
    int kernelHistogram[256];
	
    int** columnHistograms;
    columnHistograms = malloc(sizeof(int) * width);

    for (int i = 0; i < width; i++) {

        columnHistograms[i] = malloc(sizeof(int) * 256);

        for (int k = 0; k < 256; k++) {
            columnHistograms[i][k] = 0;
        }
    }

    for (int j = 0; j < width; j++) {
        for (int i = 0; i < kernel - 1; i++) {
            columnHistograms[j][[self getPixel:bitmap :width :height :i :j]]++;
        }
    }
    
    for (int row = 0; row < height; row++) {
        
		for (int col = 0; col < kernel; col++) {
            
			if ((row - kernel - 1) > 0 && (col + kernel) < width) {
                columnHistograms[col][[self getPixel:bitmap 
                                                    :width 
                                                    :height 
                                                    :row - kernel - 1 
                                                    :col]]--;
            }
            
            if ((row + kernel) < height && (col + kernel) < width) {
                columnHistograms[col][[self getPixel:bitmap 
                                                    :width 
                                                    :height 
                                                    :row + kernel 
                                                    :col]]++;
            }
		}
		
        for (int k = 0; k < 256; k++) {
            kernelHistogram[k] = 0;
        }
        
		for (int col = 0; col <= kernel; col++) {
			for (int k = 0; k < 256; k++) {
                kernelHistogram[k] += columnHistograms[col][k];
            }
        }
		
		for (int col = 0; col < width; col++) {
            
            unsigned long position = [self getPosition:width :height :row :col];
            dest_bitmap[position] = [self medianH:kernelHistogram];
            
            
			
			if ((row - kernel - 1) > 0 && (col + kernel) < width) {
                columnHistograms[col+kernel][[self getPixel:bitmap 
                                                           :width 
                                                           :height 
                                                           :row - kernel - 1
                                                           :col + kernel]]--;
            }
			if ((row + kernel) < height && (col + kernel) < width) {
                columnHistograms[col+kernel][[self getPixel:bitmap 
                                                           :width 
                                                           :height 
                                                           :row + kernel
                                                           :col + kernel]]++;
            }
            
			for (int k = 0; k < 256; k++) {
				if (col-kernel-1 > 0) {
					kernelHistogram[k] -= columnHistograms[col-kernel-1][k];
                }
                
				if (col+kernel < width) {
					kernelHistogram[k] += columnHistograms[col+kernel][k];
                }
			}
		}
	}
    
    
    for(int i = 0; i < width; i++) {
        free(columnHistograms[i]);
    }
    free(columnHistograms);
    
    
    const vImage_Buffer dstBuffer = { dest_bitmap, height, width, bytesPerRow };
    UIImage *dst = [self createImageFromPlanar8:dstBuffer];
    
    // release the memory
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(bitmap);
    free(dest_bitmap);
    
    return dst;
}

- (UIImage *)processHoughCircleTransform :(UIImage *)src :(int[6])values
{
    FURBAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    UIImage *dst = [appDelegate.imageProcessor houghCircleTransform:src :values];
    
    return dst;
}

- (UIImage *)normalize :(UIImage *)src 
                       :(int [6])values 
                       :(int [256][100])irisRectangle 
                       :(BOOL[256][100])irisMask
{
    CGImageRef imageCGSource = [src CGImage];
    
    size_t width  = CGImageGetWidth(imageCGSource);
    size_t height = CGImageGetHeight(imageCGSource);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    Pixel_8 *bitmap = (Pixel_8 *)malloc(width * height * sizeof(Pixel_8));
    
    long bytesPerPixel = 1;
    long bytesPerRow = bytesPerPixel * width;
    long bitsPerComponent = 8;
    
    CGContextRef context = CGBitmapContextCreate(bitmap, 
                                                 width, 
                                                 height, 
                                                 bitsPerComponent, 
                                                 bytesPerRow, 
                                                 colorSpace, 
                                                 kCGImageAlphaNone);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageCGSource);
    
    // my dest image/bitmap
    size_t      dest_width   = 256;
    size_t      dest_height  = 100;
    Pixel_8     *dest_bitmap = (Pixel_8 *)malloc(dest_width * dest_height * sizeof(Pixel_8));
    
    long dest_bytesPerPixel     = 1;
    long dest_bytesPerRow       = dest_bytesPerPixel * dest_width;
    
    int ang_resolution = 256;
    int rad_resolution = 100;
    
    /*             theta (256) ->
        0  0   0   0   0   0   0   0   0   0   0   0
     r  0  0   0   0   0   0   0   0   0   0   0   0
        0  0   0   0   0   0   0   0   0   0   0   0
     |  0  0   0   0   0   0   0   0   0   0   0   0
     V  0  0   0   0   0   0   0   0   0   0   0   0
        0  0   0   0   0   0   0   0   0   0   0   0
        0  0   0   0   0   0   0   0   0   0   0   0 */
    
    float x_pupil = values[0];
    float y_pupil = values[1];
    float r_pupil = values[2];
    float x_iris  = values[3];
    float y_iris  = values[4];
    float r_iris  = values[5];
    
    float ox = x_pupil - x_iris;
    float oy = y_pupil - y_iris;
    
    float sgn = 0;
    if (ox <= 0) {
        sgn = -1;
    } else if (ox > 0) {
        sgn = 1;
    }
    if (ox == 0 && oy > 0) {
        sgn = 1;
    }
    
    float phi = 0;
    if (ox == 0) {
        phi = M_PI_2;
    } else {
        phi = atanf(oy/ox);
    }
    
    // r    representa a distancia entre as bordas da
    //      pupila e da iris para um determinado angulo
    float r = 0;
    
    float a = powf(ox, 2) + powf(oy, 2);
    float b = 0;
    
    FURBPoint2D *point;
    Pixel_8 pix;
    
    // Inicializando os vetores
    for (int i = 0; i < ang_resolution; i++) {
        for (int j = 0; j < rad_resolution; j++) {
            irisRectangle[i][j] = 0;
        }
    }
    for (int i = 0; i < ang_resolution; i++) {
        for (int j = 0; j < rad_resolution; j++) {
            irisMask[i][j] = YES;
        }
    }
    
    int pupilThreshold = [self pupilThreshold:src];
    
    for (int rtheta = 0; rtheta < ang_resolution; rtheta++) {
        
        float theta = rtheta * (M_PI / (ang_resolution/2));
        b = sgn * cosf(M_PI - phi - theta);
        
        r = ( sqrtf(a)*b + sqrtf(a*powf(b, 2) - (a - powf(r_iris, 2))) );
        r = r - r_pupil;
        
        float jump = r / (rad_resolution + 2);  
                                                // Adiciona-se dois na
                                                // resolucao radial para
                                                // que as bordas entre
                                                // a pupila/iris/esclera
                                                // nao sejam consideradas.
        r = 0 + r_pupil;
        r += jump;                              // Pulando borda da pupila
        
        for (int p = 0; p < rad_resolution; p++) {
            
            point = [[FURBPoint2D alloc] init];
            [point setX:roundf(r * cos(theta))];
            [point setY:roundf(r * sin(theta))];
            [point setX:[point x] + x_pupil];
            [point setY:[point y] + y_pupil];
            
            pix = [self getPixel:bitmap :width :height :[point y] :[point x]];
            irisRectangle[rtheta][p] = pix;
            
            
            // Preenchimento da mascara da regiao da iris
            // YES      good bit (iris)
            // NO       bad bit  (pupila, sombrancelha, palpebra, reflexo, etc)
            float dist_p = powf([point x] - x_pupil, 2) + powf([point y] - y_pupil, 2);
            float dist_i = powf([point x] - x_iris, 2) + powf([point y] - y_iris, 2);
            
            if (!(dist_p > powf(r_pupil, 2) && dist_i < powf(r_iris, 2))) {
                // Se nao esta entre a pupila e a iris        
                
                irisMask[rtheta][p] = NO;
                if(p % 2 || rtheta % 2)
                    irisRectangle[rtheta][p] = 0;
            } else {
                
                if (pix < pupilThreshold) {
                    // Se faz parte da pupila
                    
                    irisMask[rtheta][p] = NO;
                    if(p % 2 || rtheta % 2)
                        irisRectangle[rtheta][p] = 255; 
                } else if(pix > 230) {
                    // Se faz parte da esclera
                    
                    irisMask[rtheta][p] = NO;
                    if(p % 2 || rtheta % 2)
                        irisRectangle[rtheta][p] = 0; 
                } else {
                    
                    // Deve-se ainda testar se o ponto
                    // esta dentro da parabola referente
                    // a palpebra superior e, repetir o
                    // mesmo teste para a inferior...
                    
                    irisMask[rtheta][p] = YES;
                }
            }
            
            r += jump;
        }
    }
    
    int aux = 0;
    for (int p = 0; p < rad_resolution; p++) {
        for (int rtheta = 0; rtheta < ang_resolution; rtheta++) {
            
            dest_bitmap[aux] = irisRectangle[rtheta][p];
            aux++;
        }
    }
    
    
    const vImage_Buffer destBuffer = { dest_bitmap, 100, 256, dest_bytesPerRow };
    
    UIImage *dst = [self createImageFromPlanar8:destBuffer];
    
    // release the memory
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(bitmap);
    free(dest_bitmap);
    
    return dst;

}

- (UIImage *)irisCodeRepresentation :(BOOL[2048])bitCode
                                    :(BOOL[2048])bitCodeMask
{
    /*     128 (1code 1mask)  ->
        0  0   0   0   0   0   0   0   0   0   0   0
     3  0  0   0   0   0   0   0   0   0   0   0   0
     2  0  0   0   0   0   0   0   0   0   0   0   0
        0  0   0   0   0   0   0   0   0   0   0   0
     |  0  0   0   0   0   0   0   0   0   0   0   0
     v  0  0   0   0   0   0   0   0   0   0   0   0
        0  0   0   0   0   0   0   0   0   0   0   0 */
    
    size_t      dest_width   = 128;
    size_t      dest_height  = 32;
    Pixel_8     *dest_bitmap = (Pixel_8 *)malloc(dest_width * dest_height * sizeof(Pixel_8));
    
    long dest_bytesPerPixel     = 1;
    long dest_bytesPerRow       = dest_bytesPerPixel * dest_width;
    
    for (int i = 0, j = 0; i < 2048; i++, j += 2) {
        
        if (bitCode[i] == YES) {
            dest_bitmap[j+0] = 0;
        } else {
            dest_bitmap[j+0] = 255;
        }
        
        if (bitCodeMask[i] == YES) {
            dest_bitmap[j+1] = 0;
        } else {
            dest_bitmap[j+1] = 255;
        }
    }
    
    const vImage_Buffer destBuffer = { dest_bitmap, dest_height, dest_width, dest_bytesPerRow };
    
    UIImage *dst = [self createImageFromPlanar8:destBuffer];
    
    // release the memory
    free(dest_bitmap);
    
    return dst;
}

- (FURBIrisCode *)generateIrisCode :(int [256][100])irisRectangle 
                                   :(BOOL[256][100])irisMask
                                   :(BOOL[2048])bitCode
                                   :(BOOL[2048])bitCodeMask
{
    FURBAppDelegate * appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.filterProcessor gaborImage :irisRectangle
                                            :256 
                                            :100 
                                            :irisMask 
                                            :bitCode 
                                            :bitCodeMask];
    
    return [[FURBIrisCode alloc] initWithBool:bitCode :bitCodeMask :0];
}

- (BOOL) compareToVerify    :(NSString *)leftIrisCodeString
                            :(NSString *)rightIrisCodeString
                            :(BOOL[2048])bitCode
                            :(BOOL[2048])bitCodeMask;
{
    double  threshold    = 0.3;
    double  minHDL       = 1.0;
    double  minHDR       = 1.0;
    
    minHDL = [self getMinHD:leftIrisCodeString  :bitCode :bitCodeMask];
    minHDR = [self getMinHD:rightIrisCodeString :bitCode :bitCodeMask];

    
    //NSLog(@"Eye [L]: %f", minHDL);
    //NSLog(@"Eye [R]: %f", minHDR);

    return (minHDL < threshold) || (minHDR < threshold);
}

- (FURBPerson *) identifyEye    :(BOOL[2048])bitCode
                                :(BOOL[2048])bitCodeMask
{
    FURBAppDelegate *appDelegate = (FURBAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray     *list  = [appDelegate loadEverything];
    FURBPerson  *p;
    FURBPerson  *pTemp;

    double      minHD  = 1.0;
    double      HDtemp = 0.0;
    
    for (int i = 0; i < [list count]; i++) {
        
        pTemp = (FURBPerson *) [list objectAtIndex:i];
        
        HDtemp = [self getMinHD:[pTemp leftIrisCodeString]  :bitCode :bitCodeMask];
            
        if (HDtemp < minHD) {
            
            minHD = HDtemp;
            p     = pTemp;  
        }
        
        HDtemp = [self getMinHD:[pTemp rightIrisCodeString] :bitCode :bitCodeMask];
        
        if (HDtemp < minHD) {
            
            minHD = HDtemp;
            p     = pTemp;  
        }
    }
    
    double threshold = 0.3;
    
    if (minHD < threshold)
        return p;
    
    return nil;
}

- (double) getMinHD :(NSString *)firstIrisString
                    :(BOOL[2048])bitCode
                    :(BOOL[2048])bitCodeMask
{
    FURBIrisCode *firstIris  = [[FURBIrisCode alloc] initWithString:firstIrisString];
    FURBIrisCode *secondIris;
    
    double  hamDistance = 0.0;
    double  minHD       = 1.0;
    
    for (int rotation = -56; rotation <= 56; rotation += 8) {
        
        secondIris = [[FURBIrisCode alloc] initWithBool:bitCode 
                                                       :bitCodeMask 
                                                       :rotation];
        
        hamDistance = [firstIris  compare:secondIris];
        
        if (hamDistance < minHD) {
            minHD       = hamDistance;
        }
    }
    
    return minHD;
}







#pragma mark - Private Methods

- (UIImage *)createImageFromPlanar8:(const vImage_Buffer)imageBuffer
{
    CGColorSpaceRef grayColorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(imageBuffer.data,
                                                 imageBuffer.width,
                                                 imageBuffer.height,
                                                 8,
                                                 imageBuffer.width,
                                                 grayColorSpace,
                                                 kCGImageAlphaNone);
    
    CGImageRef dstImage = CGBitmapContextCreateImage(context);
    UIImage *dst = [[UIImage alloc] initWithCGImage:dstImage];
    
    // release memory
    CGContextRelease(context);
    CGColorSpaceRelease(grayColorSpace);
    CFRelease(dstImage);
    
    return dst;
}

- (int)getPixel :(Pixel_8 *)bitmap :(int)width :(int)height :(int)row :(int)col
{
    unsigned long position = [self getPosition:width :height :row :col];
    return bitmap[position];
}

- (long)getPosition :(int)width :(int)height :(int)row :(int)col
{
    return (row*width + col);
}

- (int) medianH :(int *)kernelHistogram
{
    int total  = 0;
	int median = -1;
	int count  = 0;
    
	for (int i = 0; i < 256; i++) {
        
        total += kernelHistogram[i];
    }
    
	while (count < (total-1)/2+1) {
        
		median++;
		count += kernelHistogram[median];
	}
    
	return median;
}

- (int)pupilThreshold :(UIImage *)img
{
    vImagePixelCount *histogram = [self buildHistogram:img];
    
    int pupilMax = -1;
    int pupilMaxIndex = -1;
    
    for (int i = 0; i < 90; ++i) {
        
        if(histogram[i] > pupilMax) {
            
            pupilMax = histogram[i];
            pupilMaxIndex = i;
        }
    }
    
    return pupilMaxIndex + 8;
}

- (vImagePixelCount *)buildHistogram :(UIImage *)img
{
    CGImageRef imageCGSource = [img CGImage];
    
    size_t width  = CGImageGetWidth(imageCGSource);
    size_t height = CGImageGetHeight(imageCGSource);
    
    //NSLog(@"BuildHistogram: W(%zu) H(%zu)", width, height);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    Pixel_8 *bitmap = (Pixel_8 *)malloc(width * height * sizeof(Pixel_8));
    long bytesPerPixel = 1;
    long bytesPerRow = bytesPerPixel * width;
    long bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(bitmap, 
                                                 width, 
                                                 height, 
                                                 bitsPerComponent, 
                                                 bytesPerRow, 
                                                 colorSpace, 
                                                 kCGImageAlphaNone);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageCGSource);
    const vImage_Buffer srcBuffer = { bitmap, height, width, bytesPerRow };
    
    vImagePixelCount histogram[256];
    vImageHistogramCalculation_Planar8(&srcBuffer, histogram, 0);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(bitmap);
    
    // verificar isso...
    return histogram;
}

@end
