//
//  FURBVerificationViewController.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FURBVerificationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *firstnameTF;
@property (strong, nonatomic) IBOutlet UITextField *surnameTF;
@property (strong, nonatomic) IBOutlet UIImageView *myImageView;
@property (strong, nonatomic) IBOutlet UIButton *photoButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (IBAction)backgroundTap:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;

- (IBAction)takePhoto:(id)sender;
- (IBAction)verify:(id)sender;
- (IBAction)clear:(id)sender;

@end
