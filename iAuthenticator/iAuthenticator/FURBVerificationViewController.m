//
//  FURBVerificationViewController.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FURBVerificationViewController.h"
#import "FURBIrisCaptureViewController.h"
#import "FURBAppDelegate.h"
#import "FURBIrisCode.h"
#import "FURBPerson.h"
#import "FURBUtil.h"

@interface FURBVerificationViewController ()

@end

@implementation FURBVerificationViewController
@synthesize firstnameTF;
@synthesize surnameTF;
@synthesize myImageView;
@synthesize photoButton;
@synthesize activity;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)viewDidUnload
{
    [self setFirstnameTF:nil];
    [self setSurnameTF:nil];
    [self setMyImageView:nil];
    [self setPhotoButton:nil];
    
    [self setActivity:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"captureToVerify"]) {
        
        FURBIrisCaptureViewController *irisCaptureViewController = segue.destinationViewController;
        irisCaptureViewController.temporaryImageView = myImageView;
    } 
}

- (IBAction)backgroundTap:(id)sender 
{ 
    [firstnameTF resignFirstResponder]; 
    [surnameTF resignFirstResponder]; 
}

- (IBAction)textFieldDoneEditing:(id)sender { 
    
    [sender resignFirstResponder];
}

- (IBAction)takePhoto:(id)sender {
}

- (IBAction)verify:(id)sender {
    
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    
    FURBAppDelegate *appDelegate = (FURBAppDelegate *)[[UIApplication sharedApplication] delegate];
    FURBPerson *p = ((FURBPerson *) [appDelegate loadFromName
                                     :[firstnameTF text]
                                     :[surnameTF   text]]);
    
    if (p == nil) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"No person was found in the iAuthenticator's DataBase that matches with the name you've typed." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    }
    
    int  values         [6];
    int  irisRectangle  [256][100];
    BOOL irisMask       [256][100];
    BOOL bitCode        [2048];
    BOOL bitCodeMask    [2048];
    
    UIImage      *irisSample;
    UIImage      *irisGray;
    UIImage      *irisRect;
    UIImage      *irisTemp;
    FURBIrisCode *irisCode;
    
    irisSample = myImageView.image;
    irisGray   = [[FURBUtil sharedInstance] grayscaleTransform:irisSample];
    irisTemp   = irisGray;
    irisGray   = [[FURBUtil sharedInstance] applyMedianBlur:irisGray];
    irisGray   = [[FURBUtil sharedInstance] processHoughCircleTransform:irisGray :values]; 
    
    if (values[3] == 0 || values[4] == 0 || values[5] == 0) {
        
        UIAlertView *error = [[UIAlertView alloc]
                              initWithTitle:@"Error" 
                              message:@"The iris' circle could not be found! Please, take another photo of your eye..." 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
        
        [activity stopAnimating];
        
        [error show];
        return;
    } else {
        
        if (values[0] == 0 || values[1] == 0 || values[2] == 0) {
            
            UIAlertView *error = [[UIAlertView alloc]
                                  initWithTitle:@"Error" 
                                  message:@"The pupil's circle could not be found! Please, take another photo of your eye..." 
                                  delegate:self 
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil];
            
            [activity stopAnimating];
            
            [error show];
            return;
        }
    }
    
    irisRect = [[FURBUtil sharedInstance] normalize:irisTemp 
                                                   :values
                                                   :irisRectangle
                                                   :irisMask];
    
    irisCode = [[FURBUtil sharedInstance] generateIrisCode:irisRectangle
                                                          :irisMask
                                                          :bitCode
                                                          :bitCodeMask];
    
    BOOL b = [[FURBUtil sharedInstance] compareToVerify
              :[p leftIrisCodeString]
              :[p rightIrisCodeString]
              :bitCode
              :bitCodeMask];
    
    [activity stopAnimating];
    
    UIAlertView *msg;
    if (b) {
        msg = [[UIAlertView alloc]
                 initWithTitle:@"Info" 
                 message:[[NSString alloc] 
                          initWithFormat:
                          @"The iris you've provided matches with one of %@ %@'s irises.", 
                          [firstnameTF text], 
                          [surnameTF text]]
                 delegate:self 
                 cancelButtonTitle:@"Ok" 
                 otherButtonTitles:nil];
    } else {
        msg = [[UIAlertView alloc]
               initWithTitle:@"Warn" 
               message:@"The information you've provided doesn't match." 
               delegate:self 
               cancelButtonTitle:@"Ok" 
               otherButtonTitles:nil];
    }
    
    [msg show];
}

- (IBAction)clear:(id)sender {
    
    [self backgroundTap:nil];
    [firstnameTF setText:@""];
    [surnameTF setText:@""];
    [myImageView setImage:[UIImage imageNamed:@"eye.png"]];
}

- (void) threadStartAnimating:(id)data {
    [activity startAnimating];
}


@end
