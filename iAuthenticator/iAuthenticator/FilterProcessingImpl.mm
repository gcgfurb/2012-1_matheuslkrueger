//
//  FilterProcessingImpl.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FilterProcessingImpl.h"
#import "gabortoimage.h"
#import <Accelerate/Accelerate.h>

@implementation FilterProcessingImpl

- (void)gaborImage :(int [256][100])pixels 
                   :(int)w :(int)h 
                   :(BOOL [256][100])mask 
                   :(BOOL [2048])bitCode 
                   :(BOOL [2048])bitCodeMask
{
    bool _bitCode       [2048]     ;
    bool _bitCodeMask   [2048]     ;
    bool _mask          [ 256][100];
    
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            
            if (mask[i][j] == YES)
                _mask[i][j] = true;
            else
                _mask[i][j] = false;
        }
    }
    
    for (int i = 0; i < 2048; i++) {        
        _bitCode[i]     = false;
        _bitCodeMask[i] = false;
    }
    
    gaborImage(pixels, w, h, _mask, _bitCode, _bitCodeMask);
    
    for (int i = 0; i < 2048; i++) {
        
        if (_bitCode[i] == true)
            bitCode[i] = YES;
        else
            bitCode[i] = NO;
        
        if (_bitCodeMask[i] == true)
            bitCodeMask[i] = YES;
        else
            bitCodeMask[i] = NO;
    }
}

@end