//
//  FilterProcessingProtocol.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterProcessingProtocol <NSObject>

- (void)gaborImage  :(int [256][100])pixels 
                    :(int)w :(int)h 
                    :(BOOL [256][100])mask 
                    :(BOOL [2048])bitCode 
                    :(BOOL [2048])bitCodeMask;

@end
