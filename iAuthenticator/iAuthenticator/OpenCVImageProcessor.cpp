/*
 * This file is a part of sample project which demonstrates how to use OpenCV
 * library with the XCode to write the iOS-based applications.
 * 
 * Written by Eugene Khvedchenya.
 * Distributed via GPL license. 
 * Support site: http://computer-vision-talks.com
 */

#include "OpenCVImageProcessor.h"

#include <opencv2/opencv.hpp>
#include <boost/assert.hpp>

// A helper macro
#define GIL2CV(GilView) getIplImageView(GilView)


void OpenCVImageProcessor::houghCircleTransform(boost::gil::bgr8_view_t src, 
                                                boost::gil::bgr8_view_t dst,
                                                int values[6])
{
    BOOST_ASSERT(src.dimensions() == dst.dimensions());
    
    boost::gil::gray8_image_t srcGray(src.dimensions());
    boost::gil::gray8_image_t edges(src.dimensions());
    
    IplImage srcIplImage     = GIL2CV(src);
    IplImage srcGrayIplImage = GIL2CV(srcGray._view);
    IplImage dstIplImage     = GIL2CV(dst);
    
    CvMemStorage* storage = cvCreateMemStorage(0);
    cvCvtColor(&srcIplImage, &srcGrayIplImage, CV_BGR2GRAY);
    
    double fatorMult = (double) src.dimensions()[0] / (double) 864;
    int p_minR = (int) (fatorMult *  20);
    int p_maxR = (int) (fatorMult *  80);
    int i_minR = (int) (fatorMult * 150);
    int i_maxR = (int) (fatorMult * 250);
    
    int minDis = (int) (fatorMult * 100);
    int cIma_r = (int) (fatorMult *  80);
    
    CvSeq* circles1 = cvHoughCircles(&srcGrayIplImage,      // image
                                     storage,               // circle storage
                                     CV_HOUGH_GRADIENT,     // method
                                     1,                     // dp
                                     minDis,                // minDist
                                     40,                    // param1 (canny)
                                     30,                    // param2
                                     p_minR,                // minRadius
                                     p_maxR);               // maxRadius
    
    CvSeq* circles2 = cvHoughCircles(&srcGrayIplImage,      // image
                                     storage,               // circle storage
                                     CV_HOUGH_GRADIENT,     // method
                                     1,                     // dp
                                     minDis,                // minDist
                                     55,                    // param1 (canny)
                                     55,                    // param2
                                     i_minR,                // minRadius
                                     i_maxR);               // maxRadius
    
    cvCvtColor(&srcGrayIplImage, &dstIplImage, CV_GRAY2BGR);
    
    int x_iris = 0;
    int y_iris = 0;
    int r_iris = 0;
    
    int  x_pupila = 0;
    int  y_pupila = 0;
    int  r_pupila = 0;
    
    if (circles2->total == 1) {
        /* 
         * Teoricamente se existir mais de um circulo neste
         * ponto do algoritmo, deve-se descartar a amostra,
         * pois eu nao teria como validar qual dos circulos
         * corresponderia a verdadeira iris da amostra.
         */
        
        for (int i = 0; i < circles2->total; i++) {
            
            float* p = (float*)cvGetSeqElem(circles2, i);
            cv::Point center(cvRound(p[0]), cvRound(p[1]));
            int radius = cvRound(p[2]);
            
            cvCircle(&dstIplImage, center, 3,       cvScalar(255,0,0), -1, 8, 0 );        
            cvCircle(&dstIplImage, center, radius,  CV_RGB(0,255,0),    2, 8, 0 );
            
            x_iris = center.x;
            y_iris = center.y;
            r_iris = radius;
        }
        
        long d_pupila = 0;
        
        for (int i = 0; i < circles1->total; i++) {
            
            /*
             * Apenas um circulo podera ser considerado neste ponto,
             * se existir mais de um ou ate mesmo somente um deve-se
             * fazer as seguintes validacoes:
             *
             * > Se esta dentro do circulo da iris;
             * > Se esta dentro do circulo imaginario;
             * > Neste ponto, se existir mais de um circulo, escolher
             *   o que estiver menos distante (mais proximo) do ponto
             *   central da iris.
             * 
             * Caso nao reste nenhum circulo neste ponto, deve-se
             * descartar a amostra, pois ocorreriam erros, devido
             * ao fato de eu nao ter como localizar a pupila.
             */

            float* p = (float*)cvGetSeqElem(circles1, i);
            cv::Point center(cvRound(p[0]), cvRound(p[1]));
            int radius = cvRound(p[2]);
            

            // Distancia do ponto em questao em relacao ao ponto central da iris
            long dist = powl((center.x - x_iris), 2) + 
                        powl((center.y - y_iris), 2);

            if (dist < powl(r_iris, 2)) {
                // Esta dentro do circulo da iris
                
                if (dist < powl(cIma_r, 2)) {
                    // Esta dentro do circulo imaginario
                    
                    if (x_pupila == 0 && y_pupila == 0 && r_pupila == 0) {
                        // Se for a primeira vez que ira setar os valores
                        
                        x_pupila = center.x;
                        y_pupila = center.y;
                        r_pupila = radius;
                        d_pupila = dist;
                    } else if (dist < d_pupila) {
                        // Se for algum outro circulo pertencente ao
                        // circulo imaginario, sera escolhido aquele
                        // que estiver mais proximo do ponto central
                        // da iris.
                        
                        x_pupila = center.x;
                        y_pupila = center.y;
                        r_pupila = radius;
                        d_pupila = dist;
                    }
                }
            }
        }

        cv::Point pupila(x_pupila, y_pupila);
        cvCircle(&dstIplImage, pupila, 3,        cvScalar(255,0,0), -1, 8, 0 );        
        cvCircle(&dstIplImage, pupila, r_pupila, CV_RGB(255,0,0),    2, 8, 0 );
    }
    
    values[0] = x_pupila;
    values[1] = y_pupila;
    values[2] = r_pupila;
    values[3] = x_iris;
    values[4] = y_iris;
    values[5] = r_iris;
}

IplImage OpenCVImageProcessor::getIplImageView(boost::gil::bgr8_view_t srcView)
{
  IplImage img;
  cvInitImageHeader(&img, cvSize(srcView.width(), srcView.height()), IPL_DEPTH_8U, 3, 0);
  img.imageData = (char*)&srcView.begin()[0];
  img.widthStep = srcView.xy_at(0,0).row_size();
    
  return img;
}

IplImage OpenCVImageProcessor::getIplImageView(boost::gil::gray8_view_t srcView)
{
  IplImage img;
  
  cvInitImageHeader(&img, cvSize(srcView.width(), srcView.height()), IPL_DEPTH_8U, 1, 0);
  img.imageData = (char*)&srcView.begin()[0];
  img.widthStep = srcView.xy_at(0,0).row_size();

  return img;
}
