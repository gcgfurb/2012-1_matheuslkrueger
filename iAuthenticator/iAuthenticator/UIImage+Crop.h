//
//  UIImage+Crop.h
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Crop)

- (UIImage *)crop:(CGRect)rect;

@end
