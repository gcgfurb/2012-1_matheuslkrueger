//
//  UIImage+Crop.m
//  iAuthenticator
//
//  Created by Matheus Luan Krueger on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImage+Crop.h"

@implementation UIImage (Crop)

- (UIImage *)crop:(CGRect)rect {
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    
    // Tinha que dividir o tamanho da imagem por 2 dai...
    // Ver com mais detalhes, devido ao display de retina
    if (scale > 1.0 && NO) {
        
        rect = CGRectMake(rect.origin.x    * scale, 
                          rect.origin.y    * scale, 
                          rect.size.width  * scale, 
                          rect.size.height * scale);        
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef]; 
    CGImageRelease(imageRef);
    
    return result;
}

@end
